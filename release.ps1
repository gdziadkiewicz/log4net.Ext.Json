param ($versionSuffix)
dotnet build -c Release
if( ! $? ) 
{ 
    exit $lastexitcode; 
}
dotnet pack -c Release --version-suffix "$versionSuffix"
if( ! $? ) 
{ 
    exit $lastexitcode; 
}