param ($nugetApiKey)
dotnet nuget push '**/*.nupkg' --api-key "$nugetApiKey" --source https://api.nuget.org/v3/index.json
if( ! $? ) 
{ 
    exit $lastexitcode; 
}